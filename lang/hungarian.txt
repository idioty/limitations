
# Can't build
STR_BUILD_ERROR_PLACE								:Nem építheted át máshová az épületet, csak az eredeti helyére!

STR_BUILD_ERROR_RAILDEPOS							:Nem építhetsz vasúti fűtőházat!
STR_BUILD_ERROR_ROADDEPOS							:Nem építhetsz garázst!
STR_BUILD_ERROR_WATERDEPOS							:Nem építhetsz dokkot!
STR_BUILD_ERROR_RAILSTATIONS						:Nem építhetsz vasútállomást!
STR_BUILD_ERROR_TRUCKSTOPS							:Nem építhetsz teherautó-rakoróhelyet!
STR_BUILD_ERROR_BUSSTOPS							:Nem építhetsz buszmegállót!
STR_BUILD_ERROR_WATERDOCKS							:Nem építhetsz kikötőt!
STR_BUILD_ERROR_AIRPORTS							:Nem építhetsz repülőteret!

STR_BUILD_ERROR_MORE_RAILDEPOS						:Nem építhetsz több vasúti fűtőházat!
STR_BUILD_ERROR_MORE_ROADDEPOS						:Nem építhetsz több garázst!
STR_BUILD_ERROR_MORE_WATERDEPOS						:Nem építhetsz több dokkot!
STR_BUILD_ERROR_MORE_RAILSTATIONS					:Nem építhetsz több vasútállomást!
STR_BUILD_ERROR_MORE_TRUCKSTOPS						:Nem építhetsz több teherautó-rakoróhelyet!
STR_BUILD_ERROR_MORE_BUSSTOPS						:Nem építhetsz több buszmegállót!GSText
STR_BUILD_ERROR_MORE_WATERDOCKS						:Nem építhetsz több kikötőt!
STR_BUILD_ERROR_MORE_AIRPORTS						:Nem építhetsz több repülőteret!

# példa: Nem építhetsz csak '5' 'fűtőházat'.
STR_BUILD_ERROR_NUM_RAILDEPOS						:Nem építhetsz csak {NUM} vasúti fűtőházat!
STR_BUILD_ERROR_NUM_ROADDEPOS						:Nem építhetsz csak {NUM} garázst!
STR_BUILD_ERROR_NUM_WATERDEPOS						:Nem építhetsz csak {NUM} dokkot!
STR_BUILD_ERROR_NUM_RAILSTATIONS					:Nem építhetsz csak {NUM} vasútállomást!
STR_BUILD_ERROR_NUM_TRUCKSTOPS						:Nem építhetsz csak {NUM} teherautó-rakoróhelyet!
STR_BUILD_ERROR_NUM_BUSSTOPS						:Nem építhetsz csak {NUM} buszmegállót!
STR_BUILD_ERROR_NUM_WATERDOCKS						:Nem építhetsz csak {NUM} kikötőt!
STR_BUILD_ERROR_NUM_AIRPORTS						:Nem építhetsz csak {NUM} repülőteret!



STR_PURCHASE_ERROR_RAILVEHICLES						:Nem vehetsz mozdonyt!
STR_PURCHASE_ERROR_TRUCKVEHICLES					:Nem vehetsz teherautót!
STR_PURCHASE_ERROR_BUSVEHICLES						:Nem vehetsz buszt!
STR_PURCHASE_ERROR_WATERVEHICLES					:Nem vehetsz hajót!
STR_PURCHASE_ERROR_AIRVEHICLES						:Nem vehetsz repülőgépet!

STR_PURCHASE_ERROR_MORE_RAILVEHICLES				:Nem vehetsz több mozdonyt!
STR_PURCHASE_ERROR_MORE_TRUCKVEHICLES				:Nem vehetsz több teherautót!
STR_PURCHASE_ERROR_MORE_BUSVEHICLES					:Nem vehetsz több buszt!
STR_PURCHASE_ERROR_MORE_WATERVEHICLES				:Nem vehetsz több hajót!
STR_PURCHASE_ERROR_MORE_AIRVEHICLES					:Nem vehetsz több repülőgépet!



# példa: Nem vehetsz csak '5' 'mozdonyt'.
STR_PURCHASE_ERROR_NUM_RAILVEHICLES					:Nem vehetsz csak {NUM} mozdonyt!
STR_PURCHASE_ERROR_NUM_TRUCKVEHICLES				:Nem vehetsz csak {NUM} teherautót!
STR_PURCHASE_ERROR_NUM_BUSVEHICLES					:Nem vehetsz csak {NUM} buszt!
STR_PURCHASE_ERROR_NUM_WATERVEHICLES				:Nem vehetsz csak {NUM} hajót!
STR_PURCHASE_ERROR_NUM_AIRVEHICLES					:Nem vehetsz csak {NUM} repülőgépet!
