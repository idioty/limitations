/*

local list = [];
list.append();
list.remove();
list.pop();
list.sort();
list.len();	// ez a count

*/

require("LCompany.nut");

class Limitations extends GSController {
	testenabled = false;

	loaded_game = false;

	companies = null; // ebben tarolom az osszes vallalatot

	// kezdeti lehetosegek valtozoi
	initialPossibilities = {};

	// uzenetek azonositoja
	message_counter = 10; // fenntartjuk statikus uzeneteknek az elso par helyet....

	// passenger cargo azonositoja
	cargotype_passengers = null;


	constructor() {
		this.companies = [];

		this.initialPossibilities = {
			shouldRailDepos = -1,
			shouldRoadDepos = -1,
			shouldWaterDepos = -1,
			shouldRailStations = -1,
			shouldTruckStops = -1,
			shouldBusStops = -1,
			shouldWaterDocks = -1,
			shouldAirPorts = -1,
			shouldRailVehicles = -1,
			shouldTruckVehicles = -1,
			shouldBusVehicles = -1,
			shouldWaterVehicles = -1,
			shouldAirVehicles = -1,
		}
	}
}

function Limitations::Start() {
	/* Wait for the game to start */
	this.Sleep(1);

	// megkeressuk a passenger cargot
	local cargos = GSCargoList();
	local i = 0;
	foreach (cargo_id, _ in cargos) {
		if (GSCargo.GetCargoLabel(cargo_id) == "PASS") {
			this.cargotype_passengers = cargo_id;
			break;
		}
	}
	
	if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
		GSLog.Info("[Limitations::Start] GameScript started");
	
	local runAllAtHour = true; // ezt kell falsera allitani a csak multiplayerben jatszashoz
	if (runAllAtHour || GSGame.IsMultiplayer()) {
		if (this.loaded_game == false) {
			// ha uj jatet kezdunk...
			this.LoadPreferences();

		}
		else {
			if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
				GSLog.Warning("[Limitations::Start] Loaded saved game...");
		}
		
		while (true) {
			local ticks = GSController.GetTick();
			
			this.HandleEvents();
			this.UpdateCompanies();

			// Loop with a frequency of five days
			local ticks_used = GSController.GetTick() - ticks;
			local day = 1;
			local allticks = (day * 74) - ticks_used;
			if (allticks < 1)
				allticks = 1;

			this.Sleep(allticks);
		}
	}
}

function Limitations::HandleEvents() {
	if (GSEventController.IsEventWaiting()) {
		local ev = GSEventController.GetNextEvent();
		
		switch (ev.GetEventType()) {
			case GSEvent.ET_COMPANY_NEW:
				local instance = GSEventCompanyNew.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;

				local company = LCompany(company_id);
				company.setdelegate(this);
				company.SetPossible(this.initialPossibilities);

				// itt most hozzaadom, mert a kovetkezo funkcioban szukseg van a listara
				this.companies.append(company);

				break;
				
			case GSEvent.ET_COMPANY_BANKRUPT:
				local instance = GSEventCompanyBankrupt.Convert(ev);
				local company_id = instance.GetCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
				local company = this.GetCompany(company_id);
				if (company == null) break;
				
				this.RemoveCompany(company);
				company.ResetCompany();

				break;
				
				
			case GSEvent.ET_COMPANY_MERGER:
				local instance = GSEventCompanyMerger.Convert(ev);
				local company_id = instance.GetOldCompanyID();
				if (company_id == GSCompany.COMPANY_INVALID) break;
				local company = this.GetCompany(company_id);
				if (company == null) break;
				
				
				local new_company_id = instance.GetNewCompanyID();
				local newCompName = this.CompanyName(new_company_id);
				
				this.RemoveCompany(company);
				company.ResetCompany();

				break;
		}
		
	}
}

function Limitations::LoadPreferences() {
	if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
		GSLog.Info("[Limitations::LoadPreferences] Loading preferences...");

	// possible betoltes
	if ((GSController.GetSetting("shouldRailDeposMore") != -1) && (GSController.GetSetting("shouldRailDeposNum") != -1))
		if (!GSController.GetSetting("shouldRailDeposMore"))
			this.initialPossibilities.shouldRailDepos = GSController.GetSetting("shouldRailDeposNum");
	if ((GSController.GetSetting("shouldRoadDeposMore") != -1) && (GSController.GetSetting("shouldRoadDeposNum") != -1))
		if (!GSController.GetSetting("shouldRoadDeposMore"))
			this.initialPossibilities.shouldRoadDepos = GSController.GetSetting("shouldRoadDeposNum");
	if ((GSController.GetSetting("shouldWaterDeposMore") != -1) && (GSController.GetSetting("shouldWaterDeposNum") != -1))
		if (!GSController.GetSetting("shouldWaterDeposMore"))
			this.initialPossibilities.shouldWaterDepos = GSController.GetSetting("shouldWaterDeposNum");
	if ((GSController.GetSetting("shouldRailStationsMore") != -1) && (GSController.GetSetting("shouldRailStationsNum") != -1))
		if (!GSController.GetSetting("shouldRailStationsMore"))
			this.initialPossibilities.shouldRailStations = GSController.GetSetting("shouldRailStationsNum");
	if ((GSController.GetSetting("shouldTruckStopsMore") != -1) && (GSController.GetSetting("shouldTruckStopsNum") != -1))
		if (!GSController.GetSetting("shouldTruckStopsMore"))
			this.initialPossibilities.shouldTruckStops = GSController.GetSetting("shouldTruckStopsNum");
	if ((GSController.GetSetting("shouldBusStopsMore") != -1) && (GSController.GetSetting("shouldBusStopsNum") != -1))
		if (!GSController.GetSetting("shouldBusStopsMore"))
			this.initialPossibilities.shouldBusStops = GSController.GetSetting("shouldBusStopsNum");
	if ((GSController.GetSetting("shouldWaterDocksMore") != -1) && (GSController.GetSetting("shouldWaterDocksNum") != -1))
		if (!GSController.GetSetting("shouldWaterDocksMore"))
			this.initialPossibilities.shouldWaterDocks = GSController.GetSetting("shouldWaterDocksNum");
	if ((GSController.GetSetting("shouldAirPortsMore") != -1) && (GSController.GetSetting("shouldAirPortsNum") != -1))
		if (!GSController.GetSetting("shouldAirPortsMore"))
			this.initialPossibilities.shouldAirPorts = GSController.GetSetting("shouldAirPortsNum");
	if ((GSController.GetSetting("shouldRailVehiclesMore") != -1) && (GSController.GetSetting("shouldRailVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldRailVehiclesMore"))
			this.initialPossibilities.shouldRailVehicles = GSController.GetSetting("shouldRailVehiclesNum");
	if ((GSController.GetSetting("shouldTruckVehiclesMore") != -1) && (GSController.GetSetting("shouldTruckVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldTruckVehiclesMore"))
			this.initialPossibilities.shouldTruckVehicles = GSController.GetSetting("shouldTruckVehiclesNum");
	if ((GSController.GetSetting("shouldBusVehiclesMore") != -1) && (GSController.GetSetting("shouldBusVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldBusVehiclesMore"))
			this.initialPossibilities.shouldBusVehicles = GSController.GetSetting("shouldBusVehiclesNum");
	if ((GSController.GetSetting("shouldWaterVehiclesMore") != -1) && (GSController.GetSetting("shouldWaterVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldWaterVehiclesMore"))
			this.initialPossibilities.shouldWaterVehicles = GSController.GetSetting("shouldWaterVehiclesNum");
	if ((GSController.GetSetting("shouldAirVehiclesMore") != -1) && (GSController.GetSetting("shouldAirVehiclesNum") != -1))
		if (!GSController.GetSetting("shouldAirVehiclesMore"))
			this.initialPossibilities.shouldAirVehicles = GSController.GetSetting("shouldAirVehiclesNum");


	if (this.testenabled) {
		GSLog.Warning("[Limitations::LoadPreferences] Proba beallitasokat ki kell kapcsolni.");

		this.initialPossibilities.shouldRailDepos = 1;
	}
}

function Limitations::UpdateCompanies() {
	// ellenorizzuk, hogy mit szabad es mit nem vasarolni vagy epiteni
	foreach (company in this.companies) {
		if (company.company_id == GSCompany.COMPANY_INVALID) continue;

		company.CheckAllShould();
	}
}

function Limitations::GetCompany(company_id) {
	if (company_id == null) // gyorsitas...
		return null;

	foreach (company in this.companies) {
		if (company.company_id == company_id)
			return company;
	}
	
	return null;
}

function Limitations::RemoveCompany(company) {
	for (local i = 0; i < this.companies.len(); i++) {
		if (this.companies[i].company_id == company.company_id) {
			if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
				GSLog.Info("[Limitations::RemoveCompany] Remove company: " + GSCompany.GetName(company.company_id));
			this.companies.remove(i);
//			this.UpdateGameStateStoryBoards();
			break;
		}
	}
}
function Limitations::Save() {
	if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
		GSLog.Info("[Limitations::Save] Saving game...");

	local company_save_list = [];
	foreach(company in this.companies)
	company_save_list.append(company.SaveToTable());

	return {
		testenabled = this.testenabled,
		cargotype_passengers = this.cargotype_passengers,
		message_counter = this.message_counter,
		initialPossibilities = this.initialPossibilities,
		company_list = company_save_list,
	};
}

function Limitations::Load(version, table) {
	if ((GSController.GetSetting("logmode") == -1) || (GSController.GetSetting("logmode") >= 2))
		GSLog.Info("[Limitations::Load] Loading game...");
	
	this.loaded_game = true;

	// Store a copy of the table from the save game
	// but do not process the loaded data yet. Wait with that to Init
	// so that OpenTTD doesn't kick us for taking too long to load.	
	local loaded_data = {}
   	foreach(key, value in table)
		loaded_data.rawset(key, value);

	this.testenabled = loaded_data.testenabled;
	this.cargotype_passengers = loaded_data.cargotype_passengers;
	this.message_counter = loaded_data.message_counter;
	this.initialPossibilities = loaded_data.initialPossibilities;

	local acompanies = loaded_data.company_list;
	foreach (companyTable in acompanies) {
		local company = LCompany(null);
		company.setdelegate(this);
		company.LoadFromTable(companyTable, version);
		company.SetPossible(this.initialPossibilities);

		this.companies.append(company);
	}
}
