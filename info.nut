
//require("main.nut");
class Limitations extends GSInfo {
	function GetAuthor()		{ return "idioty"; }
	function GetName()			{ return "Limitations"; }
	function GetDescription() 	{ return "Limitat purchases and builds"; }
	function GetVersion()		{ return 4; }
	function GetDate()			{ return "2014-12-26"; }
	function CreateInstance()	{ return "Limitations"; }
	function GetShortName()		{ return "IDLI"; }
	function GetAPIVersion()	{ return "1.2"; }
	function GetUrl()			{ return ""; }
	function GetSettings()		{
		
		/*
		CONFIG_NONE,          ez az alap
		CONFIG_RANDOM,        veletlenszeruen valaszt a min es maxbol
		CONFIG_BOOLEAN,       az ertek bool tipusu: 0 false, 1 true
		CONFIG_INGAME,        jatek alatt is megvaltoztathato ez a parameter
		CONFIG_DEVELOPER      csak akkor jelenik meg, ha a development tools aktiv
		*/
		

		// possibilities
		AddSetting({name = "logmode", description											= "Log mode", easy_value = 2, medium_value = 2, hard_value = 2, custom_value = 2, min_value = 0, max_value = 2, flags = CONFIG_INGAME});
		AddLabels("logmode", {_0 = "Show only errors", _1 = "Show errors and warnings", _2 = "Show all messages"});
		AddSetting({name = "shouldRailDeposMore", description								= "Unlimited of traindepots", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailDeposNum", description									= "Enabled number(s) of traindepo(ts)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldRoadDeposMore", description								= "Unlimited of vehiclesdepots", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRoadDeposNum", description									= "Enabled number(s) of vehiclesdepo(ts)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldWaterDeposMore", description								= "Unlimited of docks", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterDeposNum", description								= "Enabled number(s) of dock(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldRailStationsMore", description								= "Unlimited of trainstations", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailStationsNum", description								= "Enabled number(s) of trainstation(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldTruckStopsMore", description								= "Unlimited of truck loading stations", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldTruckStopsNum", description								= "Enabled number(s) of truck loading station(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldBusStopsMore", description									= "Unlimited of bus stops", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldBusStopsNum", description									= "Enabled number(s) of bus stop(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldWaterDocksMore", description								= "Unlimited of harbors", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterDocksNum", description								= "Enabled number(s) of harbor(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldAirPortsMore", description									= "Unlimited of airports", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldAirPortsNum", description									= "Enabled number(s) of airport(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldRailVehiclesMore", description								= "Unlimited of engines", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldRailVehiclesNum", description								= "Enabled number(s) of engine(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldTruckVehiclesMore", description							= "Unlimited of trucks", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldTruckVehiclesNum", description								= "Enabled number(s) of truck(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldBusVehiclesMore", description								= "Unlimited of buses", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldBusVehiclesNum", description								= "Enabled number(s) of bus(es)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldWaterVehiclesMore", description							= "Unlimited of ships", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldWaterVehiclesNum", description								= "Enabled number(s) of ship(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
		AddSetting({name = "shouldAirVehiclesMore", description								= "Unlimited of airplanes", easy_value = 1, medium_value = 1, hard_value = 0, custom_value = 1, flags = CONFIG_BOOLEAN});
		AddSetting({name = "shouldAirVehiclesNum", description								= "Enabled number(s) of airplane(s)", easy_value = 5, medium_value = 3, hard_value = 1, custom_value = 1, min_value = 0, max_value = 1000, flags = 0});
	}
}

RegisterGS(Limitations());
