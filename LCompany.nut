//MESSAGE_Competition_ALL_COMPANY <- 0;
//MESSAGE_


class LCompany {
	mainnut = null;

	company_id = null;
	vehicles = null;
	
	stations = null;
	towns = null;
	
	// depos
	shouldRailDepos = -1;
	railDepos = null;
	shouldRoadDepos = -1;
	roadDepos = null;
	shouldWaterDepos = -1;
	waterDepos = null;
	
	// stations
	shouldRailStations = -1;
	railStations = null;
	shouldTruckStops = -1;
	truckStops = null;
	shouldBusStops = -1;
	busStops = null;
	shouldWaterDocks = -1;
	waterDocks = null;
	shouldAirPorts = -1;
	airPorts = null;
	
	// vehicles
	shouldRailVehicles = -1;
	railVehicles = null;
	shouldTruckVehicles = -1;
	truckVehicles = null;
	shouldBusVehicles = -1;
	busVehicles = null;
	shouldWaterVehicles = -1;
	waterVehicles = null;
	shouldAirVehicles = -1;
	airVehicles = null;


	// messages
	last_error_build_message = -1;
	
	// aztan a contructor
	constructor (a_company_id) {
		this.company_id = a_company_id;

		this.vehicles = [];
		this.stations = [];
		this.towns = [];
		
		// depos
		this.shouldRailDepos = -1;
		this.railDepos = [];
		this.shouldRoadDepos = -1;
		this.roadDepos = [];
		this.shouldWaterDepos = -1;
		this.waterDepos = [];
		
		// station
		this.shouldRailStations = -1;
		this.railStations = [];
		this.shouldTruckStops = -1;
		this.truckStops = [];
		this.shouldBusStops = -1;
		this.busStops = [];
		this.shouldWaterDocks = -1;
		this.waterDocks = [];
		this.shouldAirPorts = -1;
		this.airPorts = [];
		
		// vehicles
		this.shouldRailVehicles = -1;
		this.railVehicles = [];
		this.shouldTruckVehicles = -1;
		this.truckVehicles = [];
		this.shouldBusVehicles = -1;
		this.busVehicles = [];
		this.shouldWaterVehicles = -1;
		this.waterVehicles = [];
		this.shouldAirVehicles = -1;
		this.airVehicles = [];
	}
	
	// aztan az egyeb funkciok
}

function LCompany::SaveToTable() {
	return {
		company_id = this.company_id,

		vehicles = this.vehicles,
		stations = this.stations,
		towns = this.towns,

		// depos
		shouldRailDepos = this.shouldRailDepos,
		railDepos = this.railDepos,
		shouldRoadDepos = this.shouldRoadDepos,
		roadDepos = this.roadDepos,
		shouldWaterDepos = this.shouldWaterDepos,
		waterDepos = this.waterDepos,
		
		// stations
		shouldRailStations = this.shouldRailStations,
		railStations = this.railStations,
		shouldTruckStops = this.shouldTruckStops,
		truckStops = this.truckStops,
		shouldBusStops = this.shouldBusStops,
		busStops = this.busStops,
		shouldWaterDocks = this.shouldWaterDocks,
		waterDocks = this.waterDocks,
		shouldAirPorts = this.shouldAirPorts,
		airPorts = this.airPorts,
		
		// vehicles
		shouldRailVehicles = this.shouldRailVehicles,
		railVehicles =  this.railVehicles,
		shouldTruckVehicles = this.shouldTruckVehicles,
		truckVehicles = this.truckVehicles,
		shouldBusVehicles = this.shouldBusVehicles,
		busVehicles = this.busVehicles,
		shouldWaterVehicles =  this.shouldWaterVehicles,
		waterVehicles =  this.waterVehicles,
		shouldAirVehicles = this.shouldAirVehicles,
		airVehicles = this.airVehicles,
	}
}

function LCompany::LoadFromTable(table, version) {
	// valami
	this.company_id = table.company_id;
	
	this.vehicles = table.vehicles;
	this.stations = table.stations;
	this.towns = table.towns;

	// depos
	this.shouldRailDepos = table.shouldRailDepos;
	this.railDepos = table.railDepos;
	this.shouldRoadDepos = table.shouldRoadDepos;
	this.roadDepos = table.roadDepos;
	this.shouldWaterDepos = table.shouldWaterDepos;
	this.waterDepos = table.waterDepos;
	
	// stations
	this.shouldRailStations = table.shouldRailStations;
	this.railStations = table.railStations;
	this.shouldTruckStops = table.shouldTruckStops;
	this.truckStops = table.truckStops;
	this.shouldBusStops = table.shouldBusStops;
	this.busStops = table.busStops;
	this.shouldWaterDocks = table.shouldWaterDocks;
	this.waterDocks = table.waterDocks;
	this.shouldAirPorts = table.shouldAirPorts;
	this.airPorts = table.airPorts;
	
	// vehicles
	this.shouldRailVehicles = table.shouldRailVehicles;
	this.railVehicles =  table.railVehicles;
	this.shouldTruckVehicles = table.shouldTruckVehicles;
	this.truckVehicles = table.truckVehicles;
	this.shouldBusVehicles = table.shouldBusVehicles;
	this.busVehicles = table.busVehicles;
	this.shouldWaterVehicles =  table.shouldWaterVehicles;
	this.waterVehicles =  table.waterVehicles;
	this.shouldAirVehicles = table.shouldAirVehicles;
	this.airVehicles = table.airVehicles;
}

// - # FGCompany init
function LCompany::setdelegate(delegte) {
	this.mainnut = delegte;
}

function LCompany::GetCompanyMode() {
	for (local company_id_t = GSCompany.COMPANY_FIRST; company_id_t <= GSCompany.COMPANY_LAST; company_id_t++) {
		local a_company_id = GSCompany.ResolveCompanyID(company_id_t);
		if (a_company_id == GSCompany.COMPANY_INVALID) continue;
		if (a_company_id == this.company_id) {
			return company_id_t;
			break;
		}
	}
	
	GSLog.Warning("Invalid (bankrupt???) company?");
	return null;
}

function LCompany::SetPossible(possibleTable) {
	this.shouldRailDepos = possibleTable.shouldRailDepos;
	this.shouldRoadDepos = possibleTable.shouldRoadDepos;
	this.shouldWaterDepos = possibleTable.shouldWaterDepos;
	this.shouldRailStations = possibleTable.shouldRailStations;
	this.shouldTruckStops = possibleTable.shouldTruckStops;
	this.shouldBusStops = possibleTable.shouldBusStops;
	this.shouldWaterDocks = possibleTable.shouldWaterDocks;
	this.shouldAirPorts = possibleTable.shouldAirPorts;
	this.shouldRailVehicles = possibleTable.shouldRailVehicles;
	this.shouldTruckVehicles = possibleTable.shouldTruckVehicles;
	this.shouldBusVehicles = possibleTable.shouldBusVehicles;
	this.shouldWaterVehicles = possibleTable.shouldWaterVehicles;
	this.shouldAirVehicles = possibleTable.shouldAirVehicles;
}

function LCompany::ResetCompany() {
	this.SetPossible(this.mainnut.initialPossibilities);

	// depos
	this.railDepos.clear();
	this.roadDepos.clear();
	this.waterDepos.clear();
	
	// stations
	this.railStations.clear();
	this.truckStops.clear();
	this.busStops.clear();
	this.waterDocks.clear();
	this.airPorts.clear();
	
	// vehicles
	this.railVehicles.clear();
	this.truckVehicles.clear();
	this.busVehicles.clear();
	this.waterVehicles.clear();
	this.airVehicles.clear();
}
/***********************************************************************************************************************************************
 
 send
 
 ***********************************************************************************************************************************************/

function LCompany::SendErrorMessage(text) {
	if (this.last_error_build_message >= 0)
		GSGoal.CloseQuestion(this.last_error_build_message);
	this.last_error_build_message = this.mainnut.message_counter++;
	if (GSCompany.ResolveCompanyID(company_id) != GSCompany.COMPANY_INVALID) // lehet, hogy kozben brankrupted lett a company
		GSGoal.Question(this.last_error_build_message, this.company_id, text, GSGoal.QT_ERROR, GSGoal.BUTTON_CLOSE);
//	GSGoal.Question(this.mainnut.message_counter++, this.company_id, text, GSGoal.QT_ERROR, GSGoal.BUTTON_CLOSE);
	if (GSError.GetLastErrorString() != "ERR_NONE")
		GSLog.Error(GSError.GetLastErrorString());
}

function LCompany::SendState(text) {
	GSGoal.Question(this.mainnut.message_counter++, this.company_id, text, GSGoal.QT_INFORMATION, GSGoal.BUTTON_CLOSE);
	if (GSError.GetLastErrorString() != "ERR_NONE")
		GSLog.Error(GSError.GetLastErrorString());
}

// - # should support

/***********************************************************************************************************************************************
 
 should support
 
 ***********************************************************************************************************************************************/

// should_type: az az FGAward.AT_x
function LCompany::GetShouldMoreNum(should_type) {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	
	local companymode = GSCompanyMode(cstr);
	
	switch (should_type) {
		case FGAward.AT_SHOULD_RAILDEPOS:
			if (this.shouldRailDepos < 1) {
				return this.shouldRailDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_RAIL);
				if (deposList.Count() > this.shouldRailDepos)
					return 0;
				else
					return (this.shouldRailDepos - deposList.Count());
			}
			break;
		case FGAward.AT_SHOULD_ROADDEPOS:
			if (this.shouldRoadDepos < 1) {
				return this.shouldRoadDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_ROAD);
				if (deposList.Count() > this.shouldRoadDepos)
					return 0;
				else
					return (this.shouldRoadDepos - deposList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERDEPOS:
			if (this.shouldWaterDepos < 1) {
				return this.shouldWaterDepos;
			}
			else {
				local deposList = GSDepotList(GSTile.TRANSPORT_WATER);
				if (deposList.Count() > this.shouldWaterDepos)
					return 0;
				else
					return (this.shouldWaterDepos - deposList.Count());
			}
			break;
			
			// stations
		case FGAward.AT_SHOULD_RAILSTATIONS:
			if (this.shouldRailStations < 1) {
				return this.shouldRailStations;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_TRAIN);
				if (stationList.Count() > this.shouldRailStations)
					return 0;
				else
					return (this.shouldRailStations - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_TRUCKSTOPS:
			if (this.shouldTruckStops < 1) {
				return this.shouldTruckStops;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_TRUCK_STOP);
				if (stationList.Count() > this.shouldTruckStops)
					return 0;
				else
					return (this.shouldTruckStops - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_BUSSTOPS:
			if (this.shouldBusStops < 1) {
				return this.shouldBusStops;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_BUS_STOP);
				if (stationList.Count() > this.shouldBusStops)
					return 0;
				else
					return (this.shouldBusStops - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERDOCKS:
			if (this.shouldWaterDocks < 1) {
				return this.shouldWaterDocks;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_DOCK);
				if (stationList.Count() > this.shouldWaterDocks)
					return 0;
				else
					return (this.shouldWaterDocks - stationList.Count());
			}
			break;
		case FGAward.AT_SHOULD_AIRPORTS:
			if (this.shouldAirPorts < 1) {
				return this.shouldAirPorts;
			}
			else {
				local stationList = GSStationList(GSStation.STATION_AIRPORT);
				if (stationList.Count() > this.shouldAirPorts)
					return 0;
				else
					return (this.shouldAirPorts - stationList.Count());
			}
				break;
			
			// vehicles
		case FGAward.AT_SHOULD_RAILVEHICLES:
			if (this.shouldRailVehicles < 1) {
				return this.shouldRailVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldRailVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
					if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_RAIL)
						vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldRailVehicles)
					return 0;
				else
					return (this.shouldRailVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_TRUCKVEHICLES:
			if (this.shouldTruckVehicles < 1) {
				return this.shouldTruckVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldTruckVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.mainnut.cargotype_passengers) == 0))
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldTruckVehicles)
					return 0;
				else
					return (this.shouldTruckVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_BUSVEHICLES:
			if (this.shouldBusVehicles < 1) {
				return this.shouldBusVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldBusVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.mainnut.cargotype_passengers) > 0))
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldBusVehicles)
					return 0;
				else
					return (this.shouldBusVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_WATERVEHICLES:
			if (this.shouldWaterVehicles < 1) {
				return this.shouldWaterVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldWaterVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_WATER)
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				if (vehiclesList.Count() > this.shouldWaterVehicles)
					return 0;
				else
					return (this.shouldWaterVehicles - vehiclesList.Count());
			}
			break;
		case FGAward.AT_SHOULD_AIRVEHICLES:
			if (this.shouldAirVehicles < 1) {
				return this.shouldAirVehicles;
			}
			else {
				local aList = GSVehicleList();
				
				if (aList.Count() == 0)
					return this.shouldAirVehicles;
				
				local vehiclesList = GSList();
				
				foreach (vehicle, _ in aList)
				if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_AIR)
					vehiclesList.AddItem(vehicle, vehiclesList.Count());
				
				GSLog.Error("this.shouldAirVehicles: " + vehiclesList.Count());
				if (vehiclesList.Count() > this.shouldAirVehicles)
					return 0;
				else
					return (this.shouldAirVehicles - vehiclesList.Count());
			}
			break;
	}
	
	return 0;
}

// should_type: az az FGAward.AT_x
function LCompany::GetMaxMoreNum(should_type) {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	switch (should_type) {
		case FGAward.AT_SHOULD_RAILDEPOS:
			return this.shouldRailDepos;
			break;
		case FGAward.AT_SHOULD_ROADDEPOS:
			return this.shouldRoadDepos;
			break;
		case FGAward.AT_SHOULD_WATERDEPOS:
			return this.shouldWaterDepos;
			break;
			
			// stations
		case FGAward.AT_SHOULD_RAILSTATIONS:
			return this.shouldRailStations;
			break;
		case FGAward.AT_SHOULD_TRUCKSTOPS:
			return this.shouldTruckStops;
			break;
		case FGAward.AT_SHOULD_BUSSTOPS:
			return this.shouldBusStops;
			break;
		case FGAward.AT_SHOULD_WATERDOCKS:
			return this.shouldWaterDocks;
			break;
		case FGAward.AT_SHOULD_AIRPORTS:
			return this.shouldAirPorts;
			break;
			
			// vehicles
		case FGAward.AT_SHOULD_RAILVEHICLES:
			return this.shouldRailVehicles;
			break;
		case FGAward.AT_SHOULD_TRUCKVEHICLES:
			return this.shouldTruckVehicles;
			break;
		case FGAward.AT_SHOULD_BUSVEHICLES:
			return this.shouldBusVehicles;
			break;
		case FGAward.AT_SHOULD_WATERVEHICLES:
			return this.shouldWaterVehicles;
			break;
		case FGAward.AT_SHOULD_AIRVEHICLES:
			return this.shouldAirVehicles;
			break;
	}
	
	return 0;
}

function LCompany::CheckAllShould() {
	// depos
	local result = this.CheckRailDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckRoadDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterDeposShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	
	// stations, stops, docks, airports
	local result = this.CheckRailStationsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckTruckStopsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckBusStopsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterDocksShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckAirPortsShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	
	// vehicles
	local result = this.CheckRailVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckTruckVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckBusVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckWaterVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
	local result = this.CheckAirVehiclesShould();
	if (result != null)
		this.SendErrorMessage(result);
	
}

// Rail depos
function LCompany::CheckRailDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_RAIL);
	
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldRailDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_RAILDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			GSRail.SetCurrentRailType(GSRail.GetRailType(depo));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailDeposShould] Érvénytelen vasút típus!");
			
			if (!GSTile.DemolishTile(depo))
				GSLog.Error("[FGCompany::CheckRailDeposShould] Vasút depó törlése sikertelen!");
		}
 
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldRailDepos > this.railDepos.len()) {
			if (deposList.Count() > this.railDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldRailDepos > this.railDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.railDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railDepos.append(adepo);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailDepos < this.railDepos.len()) {
			for (local i = this.railDepos.len() - 1; ((i > (this.shouldRailDepos - 1)) && i >= 0); i--) {
				this.railDepos.remove(i);
			}
		}
		
		// tehat most elvileg pont anny van a railDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.railDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_RAILDEPOS, this.shouldRailDepos);
			
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.railDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			GSRail.SetCurrentRailType(GSRail.GetRailType(depo));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID) // error uzenetet mindig megjelenitunk
				GSLog.Error("[FGCompany::CheckRailDeposShould] Érvénytelen vasút típus!");
			
			if (!GSTile.DemolishTile(depo))
				GSLog.Error("[FGCompany::CheckRailDeposShould] Vasút depó törlése sikertelen!");
		}

		if (removeList.len() > 0) {
			if (this.shouldRailDepos > GSDepotList(GSTile.TRANSPORT_RAIL).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_MORE_RAILDEPOS);
		}
	}
	
	return result;
}

// Road depos
function LCompany::CheckRoadDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRoadDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_ROAD);
	
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRoadDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_ROADDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(depo, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(depo, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRoadDeposShould] Érvénytelen út típus!");
			
			if (!GSRoad.RemoveRoadDepot(depo)) {
				GSLog.Warning("[FGCompany::CheckRoadDeposShould] Road depó eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(depo))
					GSLog.Error("[FGCompany::CheckRoadDeposShould] Road depó törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldRoadDepos > this.roadDepos.len()) {
			if (deposList.Count() > this.roadDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldRoadDepos > this.roadDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.roadDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.roadDepos.append(adepo);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRoadDepos < this.roadDepos.len()) {
			for (local i = this.roadDepos.len() - 1; ((i > (this.shouldRoadDepos - 1)) && i >= 0); i--)
				this.roadDepos.remove(i);
		}
		
		// tehat most elvileg pont anny van a roadDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.roadDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_ROADDEPOS, this.shouldRoadDepos);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.roadDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(depo, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(depo, rtype))
					continue;
			}
				
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRoadDeposShould] Érvénytelen út típus!");
			
			if (!GSRoad.RemoveRoadDepot(depo)) {
				GSLog.Warning("[FGCompany::CheckRoadDeposShould] Road depó eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(depo))
					GSLog.Error("[FGCompany::CheckRoadDeposShould] Road depó törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldRoadDepos > GSDepotList(GSTile.TRANSPORT_ROAD).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_ROADDEPOS);
		}
	}
	
	return result;
}

// Water depos
function LCompany::CheckWaterDeposShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterDepos < 0) // barmennyi depo lehet
		return null;
	
	local deposList = GSDepotList(GSTile.TRANSPORT_WATER);
	if (deposList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldWaterDepos == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_WATERDEPOS);
		// ekkor eltavolitjuk az osszes depot
		foreach (depo, _ in deposList) {
			if (GSMarine.IsWaterDepotTile(depo)) {
				if (!GSMarine.RemoveWaterDepot(depo)) {
					GSLog.Warning("[FGCompany::CheckWaterDeposShould] Nem sikerült eltávolítni a hajó depót!");
					if (!GSTile.DemolishTile(depo))
						GSLog.Error("[FGCompany::CheckWaterDeposShould] Hajó depó törlése sikertelen!");
				}
			}
			else
				GSLog.Error("[FGCompany::CheckWaterDeposShould] Nem hajó depó mező");
		}
		
	}
	else {
		// eloszor hozzaadjuk a depokat, ha meg lehet
		if (this.shouldWaterDepos > this.waterDepos.len()) {
			if (deposList.Count() > this.waterDepos.len()) {
				// ekkor hozzarakjuk a nem hozzaadott depokat
				foreach (adepo, _ in deposList) {
					if (this.shouldWaterDepos > this.waterDepos.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (depo in this.waterDepos) {
							if (!van) {
								if (adepo == depo) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterDepos.append(adepo);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb deponk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterDepos < this.waterDepos.len()) {
			for (local i = this.waterDepos.len() - 1; ((i > (this.shouldWaterDepos - 1)) && i >= 0); i--)
				this.waterDepos.remove(i);
		}
		
		
		// tehat most elvileg pont anny van a roadDeposban, amennyi megengedett
		// tehat most eltavolitjuk az osszes depot, ami nincs benne a listaban
		if (deposList.Count() > this.waterDepos.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_WATERDEPOS, this.shouldWaterDepos);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (adepo, _ in deposList) {
			local van = false;
			foreach (depo in this.waterDepos) {
				if (adepo == depo) {
					van = true;
				}
			}
			if (!van)
				removeList.append(adepo);
		}
		
		foreach (depo in removeList) {
			if (GSMarine.IsWaterDepotTile(depo)) {
				if (!GSMarine.RemoveWaterDepot(depo)) {
					GSLog.Warning("[FGCompany::CheckWaterDeposShould] Nem sikerült eltávolítni a hajó depót!");
					if (!GSTile.DemolishTile(depo))
						GSLog.Error("[FGCompany::CheckWaterDeposShould] Hajó depó törlése sikertelen!");
				}
			}
			else
				GSLog.Error("[FGCompany::CheckWaterDeposShould] Nem hajó depó mező");
		}

		if (removeList.len() > 0) {
			if (this.shouldWaterDepos > GSDepotList(GSTile.TRANSPORT_WATER).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_WATERDEPOS);
		}
	}
	
	return result;
}

// stations, stops, docks, airports
// railStation
function LCompany::CheckRailStationsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailStations < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_TRAIN);
	
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRailStations == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_RAILSTATIONS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			GSRail.SetCurrentRailType(GSRail.GetRailType(loc));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailStationsShould] Érvénytelen vasút típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRAIN);
			foreach (tile, _ in stationTileList) {
				if (!GSRail.RemoveRailStationTileRectangle (tile, tile, false)) {
					GSLog.Warning("[FGCompany::CheckRailStationsShould] Vasútállomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(station))
						GSLog.Error("[FGCompany::CheckRailStationsShould] Vasútállomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldRailStations > this.railStations.len()) {
			if (stationList.Count() > this.railStations.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldRailStations > this.railStations.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.railStations) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railStations.append(astation);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailStations < this.railStations.len()) {
			for (local i = this.railStations.len() - 1; ((i > (this.shouldRailStations - 1)) && i >= 0); i--)
				this.railStations.remove(i);
		}
		
		// tehat most elvileg pont anny van a railstationsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.railStations.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_RAILSTATIONS, this.shouldRailStations);
		
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.railStations) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			GSRail.SetCurrentRailType(GSRail.GetRailType(loc));
			
			if (GSRail.GetCurrentRailType() == GSRail.RAILTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckRailStationsShould] Érvénytelen vasút típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRAIN);
			foreach (tile, _ in stationTileList) {
				if (!GSRail.RemoveRailStationTileRectangle (tile, tile, false)) {
					GSLog.Warning("[FGCompany::CheckRailStationsShould] Vasútállomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(station))
						GSLog.Error("[FGCompany::CheckRailStationsShould] Vasútállomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldRailStations > GSStationList(GSStation.STATION_TRAIN).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_RAILSTATIONS);
		}
	}
	
	return result;
}

// truckStops
function LCompany::CheckTruckStopsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldTruckStops < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_TRUCK_STOP);
	
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldTruckStops == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_TRUCKSTOPS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckTruckStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRUCK_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldTruckStops > this.truckStops.len()) {
			if (stationList.Count() > this.truckStops.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldTruckStops > this.truckStops.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.truckStops) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.truckStops.append(astation);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldTruckStops < this.truckStops.len()) {
			for (local i = this.truckStops.len() - 1; ((i > (this.shouldTruckStops - 1)) && i >= 0); i--)
				this.truckStops.remove(i);
		}
		
		// tehat most elvileg pont anny van a truckStopsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.truckStops.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_TRUCKSTOPS, this.shouldTruckStops);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.truckStops) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckTruckStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_TRUCK_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckTruckStopsShould] Teherautót állomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldTruckStops > GSStationList(GSStation.STATION_TRUCK_STOP).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_TRUCKSTOPS);
		}
	}
	
	return result;
}

// busStops
function LCompany::CheckBusStopsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldBusStops < 0) // barmennyi depo lehet
		return null;

	local stationList = GSStationList(GSStation.STATION_BUS_STOP);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;

	if (this.shouldBusStops == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_BUSSTOPS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckBusStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_BUS_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckBusStopsShould] Busz állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckBusStopsShould] Busz állomás mező törlése sikertelen!");
				}
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldBusStops > this.busStops.len()) {
			if (stationList.Count() > this.busStops.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldBusStops > this.busStops.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.busStops) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.busStops.append(astation);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldBusStops < this.busStops.len()) {
			for (local i = this.busStops.len() - 1; ((i > (this.shouldBusStops - 1)) && i >= 0); i--)
				this.busStops.remove(i);
		}
		
		// tehat most elvileg pont anny van a BusStopsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.busStops.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_BUSSTOPS, this.shouldBusStops);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.busStops) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			local rtype = GSRoad.ROADTYPE_ROAD;
			if (!GSRoad.HasRoadType(loc, rtype)) {
				rtype = GSRoad.ROADTYPE_TRAM;
				if (!GSRoad.HasRoadType(loc, rtype))
					continue;
			}
			
			GSRoad.SetCurrentRoadType(rtype);
			
			if (GSRoad.GetCurrentRoadType() == GSRoad.ROADTYPE_INVALID)
				GSLog.Error("[FGCompany::CheckBusStopsShould] Érvénytelen út típus!");
			
			local stationTileList = GSTileList_StationType(station, GSStation.STATION_BUS_STOP);
			foreach (tile, _ in stationTileList) {
				if (!GSRoad.RemoveRoadStation (tile)) {
					GSLog.Warning("[FGCompany::CheckBusStopsShould] Busz állomás mező eltávolítása sikertelen!");
					if (!GSTile.DemolishTile(tile))
						GSLog.Error("[FGCompany::CheckBusStopsShould] Busz állomás mező törlése sikertelen!");
				}
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldBusStops > GSStationList(GSStation.STATION_BUS_STOP).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_BUSSTOPS);
		}
	}
	
	return result;
}

// waterDocks
function LCompany::CheckWaterDocksShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterDocks < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_DOCK);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldWaterDocks == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_WATERDOCKS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSMarine.RemoveDock(loc)) {
				GSLog.Warning("[FGCompany::CheckWaterDocksShould] Hajó dock mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckWaterDocksShould] Hajó dock mező törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldWaterDocks > this.waterDocks.len()) {
			if (stationList.Count() > this.waterDocks.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldWaterDocks > this.waterDocks.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.waterDocks) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterDocks.append(astation);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterDocks < this.waterDocks.len()) {
			for (local i = this.waterDocks.len() - 1; ((i > (this.shouldWaterDocks - 1)) && i >= 0); i--)
				this.waterDocks.remove(i);
		}
		
		// tehat most elvileg pont anny van a WaterDocksban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.waterDocks.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_WATERDOCKS, this.shouldWaterDocks);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.waterDocks) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSMarine.RemoveDock(loc)) {
				GSLog.Warning("[FGCompany::CheckWaterDocksShould] Hajó dock mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckWaterDocksShould] Hajó dock mező törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldWaterDocks > GSStationList(GSStation.STATION_DOCK).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_WATERDOCKS);
		}
	}
	
	return result;
}

// airPorts
function LCompany::CheckAirPortsShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldAirPorts < 0) // barmennyi depo lehet
		return null;
	
	local stationList = GSStationList(GSStation.STATION_AIRPORT);
	if (stationList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldAirPorts == 0) {
		result = GSText(GSText.STR_BUILD_ERROR_AIRPORTS);
		// ekkor eltavolitjuk az osszes stationt
		foreach (station, _ in stationList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSAirport.RemoveAirport(loc)) {
				GSLog.Warning("[FGCompany::CheckAirPortsShould] Repülőtér mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckAirPortsShould] Repülőtér mező törlése sikertelen!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldAirPorts > this.airPorts.len()) {
			if (stationList.Count() > this.airPorts.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (astation, _ in stationList) {
					if (this.shouldAirPorts > this.airPorts.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (station in this.airPorts) {
							if (!van) {
								if (astation == station) {
									van = true;
								}
							}
						}
						if (!van) {
							this.airPorts.append(astation);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldAirPorts < this.airPorts.len()) {
			for (local i = this.airPorts.len() - 1; ((i > (this.shouldAirPorts - 1)) && i >= 0); i--)
				this.airPorts.remove(i);
		}
		
		// tehat most elvileg pont anny van a airPortsban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (stationList.Count() > this.airPorts.len())
			result = GSText(GSText.STR_BUILD_ERROR_NUM_AIRPORTS, this.shouldAirPorts);
	
		local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
		foreach (astation, _ in stationList) {
			local van = false;
			foreach (station in this.airPorts) {
				if (astation == station) {
					van = true;
				}
			}
			if (!van)
				removeList.append(astation);
		}
		
		foreach (station in removeList) {
			local loc = GSBaseStation.GetLocation(station);
			
			if (!GSAirport.RemoveAirport(loc)) {
				GSLog.Warning("[FGCompany::CheckAirPortsShould] Repülőtér mező eltávolítása sikertelen!");
				if (!GSTile.DemolishTile(loc))
					GSLog.Error("[FGCompany::CheckAirPortsShould] Repülőtér mező törlése sikertelen!");
			}
		}

		if (removeList.len() > 0) {
			if (this.shouldAirPorts > GSStationList(GSStation.STATION_AIRPORT).Count())
				result = GSText(GSText.STR_BUILD_ERROR_PLACE);
			else
				result = GSText(GSText.STR_BUILD_ERROR_AIRPORTS);
		}
	}
	
	return result;
}

// railVehicles
function LCompany::CheckRailVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldRailVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
		if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_RAIL)
			vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldRailVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_RAILVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült eladni a vonatot!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült a vonatot a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldRailVehicles > this.railVehicles.len()) {
			if (vehiclesList.Count() > this.railVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldRailVehicles > this.railVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.railVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.railVehicles.append(avehicle);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldRailVehicles < this.railVehicles.len()) {
			for (local i = this.railVehicles.len() - 1; ((i > (this.shouldRailVehicles - 1)) && i >= 0); i--)
				this.railVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.railVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_RAILVEHICLES, this.shouldRailVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.railVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült eladni a vonatot!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckRailVehiclesShould] Nem sikerült a vonatot a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// truckVehicles
function LCompany::CheckTruckVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldTruckVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.mainnut.cargotype_passengers) == 0))
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	
	if (this.shouldTruckVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_TRUCKVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült eladni a teherautót!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült a teherautót a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldTruckVehicles > this.truckVehicles.len()) {
			if (vehiclesList.Count() > this.truckVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldTruckVehicles > this.truckVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.truckVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.truckVehicles.append(avehicle);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldTruckVehicles < this.truckVehicles.len()) {
			for (local i = this.truckVehicles.len() - 1; ((i > (this.shouldTruckVehicles - 1)) && i >= 0); i--)
				this.truckVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.truckVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_TRUCKVEHICLES, this.shouldTruckVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.truckVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült eladni a teherautót!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckTruckVehiclesShould] Nem sikerült a teherautót a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// busVehicles
function LCompany::CheckBusVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldBusVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if ((GSVehicle.GetRoadType(vehicle) == GSRoad.ROADTYPE_ROAD) && (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_ROAD) && (GSVehicle.GetCapacity(vehicle, this.mainnut.cargotype_passengers) > 0))
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	
	local result = null;
	
	if (this.shouldBusVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_BUSVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült eladni a buszt!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült a buszt a depóba küldeni!");
			}
		}
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldBusVehicles > this.busVehicles.len()) {
			if (vehiclesList.Count() > this.busVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldBusVehicles > this.busVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.busVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.busVehicles.append(avehicle);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldBusVehicles < this.busVehicles.len()) {
			for (local i = this.busVehicles.len() - 1; ((i > (this.shouldBusVehicles - 1)) && i >= 0); i--)
				this.busVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.busVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_BUSVEHICLES, this.shouldBusVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.busVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)){
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült eladni a buszt!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckBusVehiclesShould] Nem sikerült a buszt a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// watervehicles
function LCompany::CheckWaterVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldWaterVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_WATER)
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldWaterVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_WATERVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült eladni a vízi járművet!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült a vízi járművet a depóba küldeni!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldWaterVehicles > this.waterVehicles.len()) {
			if (vehiclesList.Count() > this.waterVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldWaterVehicles > this.waterVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.waterVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.waterVehicles.append(avehicle);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldWaterVehicles < this.waterVehicles.len()) {
			for (local i = this.waterVehicles.len() - 1; ((i > (this.shouldWaterVehicles - 1)) && i >= 0); i--)
				this.waterVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.waterVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_WATERVEHICLES, this.shouldWaterVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.waterVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült eladni a vízi járművet!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckWaterVehiclesShould] Nem sikerült a vízi járművet a depóba küldeni!");
				}
			}
		}
	}
	
	return result;
}

// airVehicles
function LCompany::CheckAirVehiclesShould() {
	local cstr = this.GetCompanyMode();
	if (cstr == null)
		return 0;
	local companymode = GSCompanyMode(cstr);
	
	if (this.shouldAirVehicles < 0) // barmennyi depo lehet
		return null;
	
	local aList = GSVehicleList();
	
	if (aList.Count() == 0)
		return null;
	
	local vehiclesList = GSList();
	
	foreach (vehicle, _ in aList)
	if (GSVehicle.GetVehicleType(vehicle) == GSVehicle.VT_AIR)
		vehiclesList.AddItem(vehicle, vehiclesList.Count());
	
	if (vehiclesList.Count() == 0)
		return null;
	
	local result = null;
	if (this.shouldAirVehicles == 0) {
		result = GSText(GSText.STR_PURCHASE_ERROR_AIRVEHICLES);
		// ekkor eltavolitjuk az osszes stationt
		foreach (vehicle, _ in vehiclesList) {
			if (GSVehicle.IsStoppedInDepot(vehicle)) {
				if (!GSVehicle.SellVehicle(vehicle))
					GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült eladni a repülőt!");
			}
			else {
				if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
					if (!GSVehicle.SendVehicleToDepot(vehicle))
						GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült a repülőt a hangárba küldeni!");
			}
		}
		
	}
	else {
		// eloszor hozzaadjuk a stationkat, ha meg lehet
		if (this.shouldAirVehicles > this.airVehicles.len()) {
			if (vehiclesList.Count() > this.airVehicles.len()) {
				// ekkor hozzarakjuk a nem hozzaadott stationkat
				foreach (avehicle, _ in vehiclesList) {
					if (this.shouldAirVehicles > this.airVehicles.len()) { // nehogy veletlen tobbet adjunk hozza
						local van = false;
						foreach (vehicle in this.airVehicles) {
							if (!van) {
								if (avehicle == vehicle) {
									van = true;
								}
							}
						}
						if (!van) {
							this.airVehicles.append(avehicle);
						}
					}
				}
			}
		}
		
		// ha esetleg veletlen tobb stationnk lenne elmentve, akkor eltavolitjuk a kesobbieket a listabol
		if (this.shouldAirVehicles < this.airVehicles.len()) {
			for (local i = this.airVehicles.len() - 1; ((i > (this.shouldAirVehicles - 1)) && i >= 0); i--)
				this.airVehicles.remove(i);
		}
		
		// tehat most elvileg pont anny van a railVehiclesban, amennyi megengedett
		// tehat most eltavolitjuk az osszes stationt, ami nincs benne a listaban
		if (vehiclesList.Count() > this.airVehicles.len()) {
			result = GSText(GSText.STR_PURCHASE_ERROR_NUM_AIRVEHICLES, this.shouldAirVehicles);
			local removeList = []; // biztonsag kedveert egy uj listahoz adjuk az id-ket
			foreach (avehicle, _ in vehiclesList) {
				local van = false;
				foreach (vehicle in this.airVehicles) {
					if (avehicle == vehicle) {
						van = true;
					}
				}
				if (!van)
					removeList.append(avehicle);
			}
			
			foreach (vehicle in removeList) {
				if (GSVehicle.IsStoppedInDepot(vehicle)) {
					if (!GSVehicle.SellVehicle(vehicle))
						GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült eladni a repülőgépet!");
				}
				else {
					if (!GSOrder.IsGotoDepotOrder(vehicle, GSOrder.ORDER_CURRENT))
						if (!GSVehicle.SendVehicleToDepot(vehicle))
							GSLog.Error("[FGCompany::CheckAirVehiclesShould] Nem sikerült a repólőgépet a hangárba küldeni!");
				}
			}
		}
	}
	
	return result;
}
