#!/bin/bash

#  maxminicopy.sh
#  feca
#
#  Created by Ferenc Kiss on 2012.09.28..
#

sourceDir=${SRCROOT}
echo sourceDir: "$sourceDir"
excludeFile=$sourceDir"/exclude.txt"

for aDir in "$@"
do
	targetDir="$aDir"
	echo targetDir: "$targetDir"
	rsync -u8av --exclude-from "$excludeFile" "$sourceDir" "$targetDir"
done
